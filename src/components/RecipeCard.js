import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withStyles, createStyleSheet } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Card, { CardContent } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import ThumbUpIcon from 'material-ui-icons/ThumbUp';
import ThumbDownIcon from 'material-ui-icons/ThumbDown';

import { actions } from '../actions';

const styleSheet = createStyleSheet('HomeView', () => ({
    card: {
        marginTop: 10
    },
    controls: {
        paddingLeft: 10
    },
    link: {
    	textDecoration: 'none'
    }
}));

// Use purecomponent to prevent unneccessary re-render when recipe has not changed
export class RecipeCard extends PureComponent { 
    constructor(props){
        super(props);

        // Bind functions once in constructor to prevent it generating multiple bindings in render
        this.like = this.props.like.bind(this, this.props.recipe.get('id'));
        this.dislike = this.props.dislike.bind(this, this.props.recipe.get('id'));
    }

    render(){
        const recipe = this.props.recipe.toObject();
        return (
            <Card className={this.props.classes.card}>
                <Link to={`/view/${recipe.id}`} className={this.props.classes.link}>
                    <CardContent>
                        <Typography type="headline" component="h2">{recipe.title}</Typography>
                        <Typography component="body1">{recipe.description}</Typography>
                    </CardContent>
                </Link>
                <div className={this.props.classes.controls}>    
                    <IconButton onClick={this.like}>   
                        <ThumbUpIcon />
                        <Typography component="body1">{recipe.likes}</Typography>
                    </IconButton>
                    <IconButton onClick={this.dislike}>
                        <ThumbDownIcon />
                        <Typography component="body1">{recipe.dislikes}</Typography>
                    </IconButton>
                </div>
            </Card>
        );
    }
};

export default connect(null, actions)(withStyles(styleSheet)(RecipeCard));