import React, { Component } from 'react';
import { MuiThemeProvider } from 'material-ui/styles';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { BrowserRouter,  Route } from 'react-router-dom';
import 'typeface-roboto';

import rootReducer from './reducers';
import HomeView from './views/HomeView';
import DetailView from './views/DetailView';
import AddView from './views/AddView';

let store = createStore(rootReducer);

class App extends Component {
	render() {
		return (
			<MuiThemeProvider>
				<Provider store={store}>
					<BrowserRouter>
						<div>
							<Route exact path="/" component={HomeView} />
							<Route path="/view/:id" component={DetailView} />
							<Route path="/add/" component={AddView} />
							<Route path="/edit/:id" component={AddView} />
						</div>
					</BrowserRouter>
				</Provider>
			</MuiThemeProvider>
		);
	}
}

export default App;
