export const recipes = {
    "1": {
        "id": "1",
        "title": "Crock Pot Roast",
        "description": "Moist and juicy pot roast",
        "ingredients": "1 beef roast, 1 brown gravy mix, 1 dried Italian salad dressing mix, 1 dry ranch dressing mix, 0.5 cup of water",
        "instructions": "Place beef roast in crock pot. Mix the dried mixes together in a bowl and sprinkle over the roast. Pour the water around the roast. Cook on low for 7-9 hours.",
        "likes": 10,
        "dislikes": 0
    },
    "2": {
        "id": "2",
        "title": "Roasted asparagus",
        "description": "Salty and savory, the roasting method kills the natural bitterness of asparagus",
        "ingredients": "1 lb of asparagus, 1.5 tbsp of olive oil, 0.5 tbsp of kosher salt",
        "instructions":  "Preheat oven to 425°F. Cut off the woody bottom part of the asparagus spears and discard. With a vegetable peeler, peel off the skin on the bottom 2-3 inches of the spears (this keeps the asparagus from being all.\",string.\", and if you eat asparagus you know what I mean by that). Place asparagus on foil-lined baking sheet and drizzle with olive oil. Sprinkle with salt. With your hands, roll the asparagus around until they are evenly coated with oil and salt. Roast for 10-15 minutes, depending on the thickness of your stalks and how tender you like them. They should be tender when pierced with the tip of a knife. The tips of the spears will get very brown but watch them to prevent burning. They are great plain, but sometimes I serve them with a light vinaigrette if we need something acidic to balance out our meal.",
        "likes": 8,
        "dislikes": 3
    },
    "3": {
        "id": "3",
        "title": "Curried lentils and rice",
        "description": "Carbs and protein, for cheap!",
        "ingredients": "1 quart of beef broth, 1 cup of dried green lentils, 0.5 cup of basmati rice, 1tsp of curry powder, 1tsp of salt",
        "instructions":  "Bring broth to a low boil. Add curry powder and salt. Cook lentils for 20 minutes. Add rice and simmer for 20 minutes.",
        "likes": 5,
        "dislikes": 0
    }
}