import { fromJS } from 'immutable';

import { recipes } from '../recipes.js';
import { actionTypes } from '../actions';

const initialState = fromJS({
    recipes
})

const rootReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.LIKE:
            let curLikes = state.getIn(['recipes', action.id, 'likes']);
            return state.setIn(['recipes', action.id, 'likes'], ++curLikes);
        case actionTypes.DISLIKE:
            let curDislikes = state.getIn(['recipes', action.id, 'dislikes']);
            return state.setIn(['recipes', action.id, 'dislikes'], ++curDislikes);
        case actionTypes.ADD:
            // TODO: Use a uuid lib to generate ids
            action.recipe.id = Math.floor(Math.random() * 1000).toString();
            action.recipe.likes = 0;
            action.recipe.dislikes = 0;
            const recipes = state.get('recipes').set(action.recipe.id, fromJS(action.recipe));
            return state.set('recipes', recipes);
        case actionTypes.EDIT:
            return state.setIn(['recipes', action.id], fromJS(action.recipe));
        default:
            return state;
    }
}

export default rootReducer;