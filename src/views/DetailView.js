import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import BackIcon from 'material-ui-icons/ArrowBack';
import EditIcon from 'material-ui-icons/ModeEdit';
import { withStyles, createStyleSheet } from 'material-ui/styles';

const styleSheet = createStyleSheet('DetailView', () => ({
    flex: {
        flex: 1,
    },
    content: {
        padding: 10
    },
    section: {
        marginTop: 20
    },
    link: {
    	textDecoration: 'none'
    }
}));

const DetailView = ({classes, history, recipe}) => {
    recipe = recipe.toObject();
    return (
        <div>
            <AppBar position="static" color="default">
                <Toolbar>
                    <IconButton onClick={history.goBack}>
                        <BackIcon />
                    </IconButton>
                    <Typography type="title" className={classes.flex}>{recipe.title}</Typography>
                    <Link to={`/edit/${recipe.id}`} className={classes.link}>
                        <IconButton>
                            <EditIcon />
                        </IconButton>
                    </Link>
                </Toolbar>
            </AppBar>
            <div className={classes.content}>
                <div className={classes.section}>
                    <Typography type="headline" className={classes.flex}>Description</Typography>
                    <Typography type="body1" className={classes.flex}>{recipe.description}</Typography>
                </div>
                <div className={classes.section}>
                    <Typography type="headline" className={classes.flex}>Ingredients</Typography>
                    <Typography type="body1" className={classes.flex}>{recipe.ingredients}</Typography>
                </div>
                <div className={classes.section}>
                    <Typography type="headline" className={classes.flex}>Instructions</Typography>
                    <Typography type="body1" className={classes.flex}>{recipe.instructions}</Typography>
                </div>
             </div>
        </div> 
    );
};

const mapStateToProps = (state, ownProps) => ({
    recipe: state.getIn(['recipes', ownProps.match.params.id])
})

export default connect(mapStateToProps)(withStyles(styleSheet)(DetailView));