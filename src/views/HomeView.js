import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui-icons/AddCircleOutline';
import { withStyles, createStyleSheet } from 'material-ui/styles';

import RecipeCard from '../components/RecipeCard';

const styleSheet = createStyleSheet('HomeView', () => ({
    flex: {
        flex: 1,
    },
    content: {
        paddingLeft: 10,
        paddingRight: 10
    },
    link: {
    	textDecoration: 'none'
    }
}));

const HomeView = ({classes, recipes}) => (
    <div>
        <AppBar position="static" color="default">
            <Toolbar>
                <Typography type="title" className={classes.flex}>Cookbook</Typography>
                <Link to="/add" className={classes.link}>
                    <IconButton>
                        <AddIcon />
                    </IconButton>
                </Link>
            </Toolbar>
        </AppBar>
        <div className={classes.content}>
            {recipes.map(recipe => (
                <RecipeCard key={recipe.get('id')} recipe={recipe}/>
            )).toArray()}
        </div>
    </div>
)


const mapStateToProps = state => ({
    recipes: state.get('recipes')
})

export default connect(mapStateToProps)(withStyles(styleSheet)(HomeView));