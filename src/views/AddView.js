import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import BackIcon from 'material-ui-icons/ArrowBack';
import { withStyles, createStyleSheet } from 'material-ui/styles';

import { actions } from '../actions';

const styleSheet = createStyleSheet('AddView', () => ({
    content: {
        padding: 10
    },
    button: {
        marginTop: 20
    }
}));

const blankRecipe = {
    title: '',
    description: '',
    ingredients: '',
    instructions: ''
}

export class AddView extends Component { 
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render(){
        const recipe = (this.props.recipe && this.props.recipe.toObject()) || blankRecipe;
        const classes = this.props.classes;

        return (
            <div>
                <AppBar position="static" color="default">
                    <Toolbar>
                        <IconButton onClick={this.props.history.goBack}>
                            <BackIcon />
                        </IconButton>
                        <Typography type="title" >{this.props.recipe ? 'Edit recipe' : 'Add new recipe'}</Typography>
                    </Toolbar>
                </AppBar>
                <div className={classes.content}>
                    <TextField
                        id="title"
                        label="Name"
                        margin="normal"
                        fullWidth
                        defaultValue={recipe.title}
                    />
                    <TextField
                        id="description"
                        label="Description"
                        margin="normal"
                        fullWidth
                        defaultValue={recipe.description}
                    />
                    <TextField
                        id="ingredients"
                        label="Ingredients"
                        multiline
                        rows="5"
                        margin="normal"
                        fullWidth
                        defaultValue={recipe.ingredients}
                    />
                    <TextField
                        id="instructions"
                        label="Instructions"
                        multiline
                        rows="5"
                        margin="normal"
                        fullWidth
                        defaultValue={recipe.instructions}
                    />
                    <Grid container direction="row" justify="center">
                        <Button raised color="primary" className={classes.button} onClick={this.handleSubmit}>Save</Button>
                    </Grid>
                </div>
            </div>
        )
    }

    handleSubmit(){
        const recipe = {
            title: document.getElementById('title').value,
            description: document.getElementById('description').value,
            ingredients: document.getElementById('ingredients').value,
            instructions: document.getElementById('instructions').value,
        }
        
        if(this.props.recipe){
            this.props.edit(this.props.match.params.id, recipe);
        } else {
            this.props.add(recipe);
        }
        this.props.history.goBack();
    }
};

const mapStateToProps = (state, ownProps) => ({
    recipe: state.getIn(['recipes', ownProps.match.params.id])
})

export default connect(mapStateToProps, actions)(withStyles(styleSheet)(AddView));