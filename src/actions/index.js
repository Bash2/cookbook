const LIKE = 'LIKE_RECIPE';
const DISLIKE = 'DISLIKE_RECIPE';
const ADD = 'ADD_RECIPE';
const EDIT = 'EDIT_RECIPE';

export const actionTypes = {
    LIKE,
    DISLIKE,
    ADD,
    EDIT
}

export const actions = {
    like: id => ({ type: LIKE, id}),
    dislike: id => ({ type: DISLIKE, id}),
    add: recipe => ({ type: ADD, recipe}),
    edit: (id, recipe) => ({ type: EDIT, id, recipe})
}